## Creating our current configuration -
### Step 1 -
We have to download our terraform.tfstate file from where it is located currently. ( Now It can be stored inside s3 or reo or somewhere else )
### Step 2 -
We have to go to our current github repo < paste our github repo link here >. There you can find a repo.diff file which contains the content difference between the terraform module < paste the public terraform module link here > and our current configuration.
### Step 3 -
Now we have to make our current configuration. we have to clone the public terraform github repo ( the link is maintained above ). Then have to `terraform init` Then when we open the repo.diff then we can see that there are mainly three types of lines under each file path.
- First one is without adding any negetive and positive sign before line. It means the line is unchanged
- Second one is with three negetive sign before line. It means we have to delete this line from terraform module cloned folder ( The path of specific folder is maintained in that repo.diff ) to make our 
  current configuration.
- Third one is with three positive sign before line. It means we have to add this line to terraform module cloned folder ( The path of specific folder is maintained in that repo.diff ) to make current configuration. 
**Now our current configuration is ready, Make sure that you have pasted the terraform.tfstate inside of this given location -**
`.terraform/modules/github-runner/examples/default/`
### Step 4 -
Now if we want to check that if our current configuration is currectly created or not then we have to come inside
`.terraform/modules/github-runner/examples/default/`. and run `terraform init` and `terraform plan` command. In the output if it gives `No changes. Your infrastructure matches the configuration.` Then you have successfully created out current configuration.

## Modifying our current configuration as per our requirement -
**make sure that you have follwed previous steps properly otherwise the below steps may create problem.**
 

- at the `examples/default/main.tf` we can found a module called runners.
- Inside of that module we can see our declared variables for current configuration. Suppose for our current 
configuration we are using `instance_types = ["m5.large", "c5.large"]` but we want to change those instance_types to
t2.micro and t2.nano then just needs to replace `instance_types = ["m5.large", "c5.large"]` with 
`instance_types = ["t2.micro", "t2.nano"]. 
- giving you a another example, suppose for our current configuration we are using scaling down cron example is 
`scale_down_schedule_expression = "cron(* * * * ? *)"` but now if you want to change this cron expression then we have to replace
the `scale_down_schedule_expression = "cron(* * * * ? *)"` with `scale_down_schedule_expression = "cron(<new cron expression>)"`
- In the above three points I maintained that How to do modification of those variables ( variables under `module "runners" {}` ) which values are maintained inside `terraform-aws-github-runner/examples/default/main.tf` . Now we will discuss how to override those variable's values which values come from default value of `terraform-aws-github-runner/modules/runners/variables.tf`.
- As a example in `terraform-aws-github-runner/modules/runners/variables.tf` there is a variable named `lambda_timeout_scale_down`
and the value of this is `60`. But this variable and value are not maintained in `terraform-aws-github-runner/examples/default/main.tf`.
So from `terraform-aws-github-runner/examples/default` when we do `terraform apply` then by default it catches the default
value of `lambda_timeout_scale_down` from `terraform-aws-github-runner/modules/runners/variables.tf` and which is `60`.
- So to modify our current configuration if we need to override the default value of `lambda_timeout_scale_down` then 
come to `terraform-aws-github-runner/examples/default/main.tf` and come inside `module "runners" { }` and type there
`lambda_timeout_scale_down = "<new_value>"`. Now when we do `terraform apply` then it will catch the value of lambda_timeout_scale_down as
<new_value>.

## By using above points one can modify the current configuration as per their requirements easily.